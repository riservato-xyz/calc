const harris = {
  male: (weight, height, age) => 66 + (13.7 * weight) + (5 * height) - (6.8 * age),
  female: (weight, height, age) => 655 + (9.6 * weight) + (1.8 * height) - (4.7 * age)
}

const mifflin = {
  male: (weight, height, age) => (10 * weight) + (6.25 * height) - (5.0 * age) + 5,
  female: (weight, height, age) => (10 * weight) + (6.25 * height) - (5.0 * age) - 161
}

const tinsley = (weight) => {

  return (24.8 * weight) + 10
};

const calculate = (shape, sex, weight, height, age) => {

  switch (shape) {
    case "under":
      if (sex === "male") return harris.male(weight, height, age);
      return harris.female(weight, height, age);

    case "over":
      if (sex === "male") return mifflin.male(weight, height, age);
      return mifflin.female(weight, height, age);

    default:
      return tinsley(weight);
  }
}

export { 
  calculate
}
