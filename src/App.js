import './App.css';
import { Steps, Step } from 'react-step-builder';

import Shape from './steps/shape';
import Info from './steps/info';
import Lifestyle from './steps/lifestyle';
import Result from './steps/result';

function App() {
  return (
    <div className="App">
      <Steps>
        <Step title="como está seu peso?" component={Shape} />
        <Step title="sobre você..." component={Info} />
        <Step title="nível de atividade" component={Lifestyle} />
        <Step title="resultados" component={Result} />
      </Steps>
    </div>
  );
}

export default App;
