import "./styles.css";
import logo from '../../img/logo.jpg'

import { useEffect, useState } from "react";

import { lifeStyleEnum } from '../lifestyle';

import Box from '../../components/box';
import { CardText } from '../../components/card';
import { calculate } from '../../utils/calculator';

const format = {
  kcal: (value) => Math.round(value) + " kcal"
}

const Result = (props) => {

  const {
    shape,
    sex,
    weight,
    height,
    age,
    lifestyle
  } = props.state;

  const [base, setBase] = useState(0);
  const [FA, setFA] = useState(0);

  useEffect(() => {

    const base = calculate(shape, sex, weight, height, age);
    const FA = lifeStyleEnum[lifestyle];

    setBase(base);
    setFA(FA);
  }, [base, FA, shape, sex, weight, height, age, lifestyle]);

  const buttons = { prev: "voltar", next: "reiniciar" };

  return (
    <Box filled={true} buttons={buttons} {...props}>
      <div className="result_container">
        <p className="_description">
          Com base nos seus dados, calcula-se que você possui um <b>metabolismo basal de {format.kcal(base)}</b>.
        </p>

        <div className="_logo">
            <a href="https://www.instagram.com/masselorc" target="_blank" rel="noopener noreferrer">
              <img src={logo} alt="logo rosto marcelo cortez" />
            </a>
        </div>

        <div className="_calories">
          <CardText
            featured={format.kcal(base * FA)}
            title="preservar peso"
            description="mantenha este consumo de calorias diárias."
          ></CardText>

          <CardText
            featured={format.kcal((base * FA) - 500)}
            title="perder peso"
            description="não ultrapasse esta meta de calorias diárias."
            rtl
          ></CardText>

          <CardText
            featured={format.kcal((base * FA) + 500)}
            title="ganhar massa"
            description="atinja esta meta de calorias diárias."
          ></CardText>
        </div>
      </div>
    </Box>
  );
};

export default Result;
