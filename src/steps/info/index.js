import React, { useEffect, useState } from "react";

import "./styles.css";

import Box from '../../components/box';
import {
  RadioIcon,
  InputAppend
} from '../../components/input';

import {
  FemaleIcon,
  MaleIcon
} from '../../components/icons';


const Info = (props) => {

  const [filled, setFilled] = useState(false);

  const {
    state,
    getState,
  } = props;

  useEffect(() => {

    const {
      sex,
      age,
      height,
      weight
    } = state;

    if (sex && age && height && weight) setFilled(true)
  }, [state])

  return (
    <Box filled={filled} {...props}>
      <form className="container-info">
        <fieldset className="sexFields">
          <RadioIcon
            id="sexFemale"
            name="sex"
            value="female"
            checked={state.sex === "female"}
            {...props}>

            <FemaleIcon className="--lg" />
          </RadioIcon>

          <RadioIcon
            id="sexMale"
            name="sex"
            value="male"
            checked={state.sex === "male"}
            {...props}>

            <MaleIcon className="--lg" />
          </RadioIcon>
        </fieldset>

        <fieldset className="otherFields">
          <InputAppend
            id="infoAge"
            hint="anos"
            label="Idade"
            type="text"
            name="age"
            value={getState("age", "")}
            {...props}
          />

          <InputAppend
            id="infoWeight"
            hint="kg"
            label="Peso"
            type="text"
            name="weight"
            value={getState("weight", "")}
            {...props}
          />

          <InputAppend
            id="infoHeight"
            hint="cm"
            label="Altura"
            type="text"
            name="height"
            value={getState("height", "")}
            {...props}
          />
        </fieldset>
      </form>
    </Box>
  );
};

export default Info;
