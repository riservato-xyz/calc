import React, { useEffect, useState } from "react";

import Box from '../../components/box';
import { RadioCard } from '../../components/input';

import "./styles.css";

const Shape = (props) => {

  const [filled, setFilled] = useState(false);

  const {
    state
  } = props;

  useEffect(() => {

    if ("shape" in state) setFilled(true)
  }, [state])

  return (
    <Box filled={filled} {...props}>
      <form className="shape-container">
        <RadioCard
          id="shapeUnder"
          className="option1"
          name="shape"
          value="under"
          cardTitle="baixo"
          description="Estou abaixo ou próximo do peso ideal."
          iconID="carbon:skill-level-basic"
          checked={state.shape === "under"}
          {...props}
        ></RadioCard>
        
        <RadioCard
          id="shapeOver"
          className="option2"
          name="shape"
          value="over"
          cardTitle="muito acima"
          description="Estou muito acima do peso ideal."
          iconID="carbon:skill-level-intermediate"
          checked={state.shape === "over"}
          {...props}
        ></RadioCard>
        
        <RadioCard
          id="shapeAthlete"
          className="option3"
          name="shape"
          value="athlete"
          cardTitle="atleta"
          description="Sou um atleta de alta massa magra."
          iconID="carbon:skill-level-advanced"
          checked={state.shape === "athlete"}
          {...props}
        ></RadioCard>
      </form>
    </Box>
  );
};

export default Shape;
