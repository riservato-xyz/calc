import React, { useEffect, useState } from "react";

import Box from '../../components/box';
import { RadioCard } from '../../components/input';

import "./styles.css";

const lifeStyleEnum = {
  "sedentary": 1.2,
  "brief": 1.4,
  "moderate": 1.55,
  "intense": 1.7,
  "veryIntense": 1.9,
}

const Lifestyle = (props) => {

  const [filled, setFilled] = useState(false)

  const {
    state
  } = props;

  const buttons = { prev: "voltar", next: "calcular" };

  useEffect(() => {
  
    const {
      lifestyle
    } = state;
  
    if (lifestyle) setFilled(true)
  }, [state])

  return (
    <Box filled={filled} buttons={buttons} {...props}>
      <form className="life-container">
        <RadioCard
          id="lifeSedentary"
          className="option --order-1"
          name="lifestyle"
          value="sedentary"
          cardTitle="sedentário"
          cardSubtitle={`(FA: ${lifeStyleEnum["sedentary"]})`}
          cardModifiers="--header-revert"
          description="Não pratico atividade física e passo boa parte do dia sentado ou deitado."
          iconID="emojione-v1:pizza"
          checked={state.lifestyle === "sedentary"}
          {...props}
        ></RadioCard>

        <RadioCard
          id="lifeBrief"
          className="option --order-4"
          name="lifestyle"
          value="brief"
          cardTitle="ligeira"
          cardSubtitle={`(FA: ${lifeStyleEnum["brief"]})`}
          cardModifiers="--header-revert"
          description="Faço atividade física 1 a 3 vezes por semana e não me movimento tanto ao longo do dia."
          iconID="noto:man-in-lotus-position-medium-skin-tone"
          checked={state.lifestyle === "brief"}
          {...props}
        ></RadioCard>

        <RadioCard
          id="lifeModerate"
          className="option --order-2"
          name="lifestyle"
          value="moderate"
          cardTitle="moderada"
          cardSubtitle={`(FA: ${lifeStyleEnum["moderate"]})`}
          cardModifiers="--header-revert"
          description="Faço atividade física pelo menos 4 vezes na semana e movimento razoavelmente ao longo do dia."
          iconID="noto:man-running-medium-skin-tone"
          checked={state.lifestyle === "moderate"}
          {...props}
        ></RadioCard>

        <br />

        <RadioCard
          id="lifeIntense"
          className="option --order-3"
          name="lifestyle"
          value="intense"
          cardTitle="intensa"
          cardSubtitle={`(FA: ${lifeStyleEnum["intense"]})`}
          cardModifiers="--header-revert"
          description="Faço atividade física 5 a 6 vezes por semana e me movimento consideravelmente ao longo do dia."
          iconID="noto:man-climbing-medium-skin-tone"
          checked={state.lifestyle === "intense"}
          {...props}
        ></RadioCard>

        <br />

        <RadioCard
          id="lifeVeryIntense"
          className="option --order-5"
          name="lifestyle"
          value="veryIntense"
          cardTitle="muito intensa"
          cardSubtitle={`(FA: ${lifeStyleEnum["veryIntense"]})`}
          cardModifiers="--header-revert"
          description="Faço atividade física 6 a 7 vezes por semana e me movimento bastante ao longo do dia."
          iconID="noto:man-lifting-weights-medium-skin-tone"
          checked={state.lifestyle === "veryIntense"}
          {...props}
        ></RadioCard>
      </form>
    </Box>
  );
};

export {
  lifeStyleEnum
}

export default Lifestyle;
