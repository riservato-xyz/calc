const Before = (props) => (
  <span>This component will be rendered before the Step components in every step</span>
)

const After = (props) => (
  <span>This component will be rendered after the Step components in every step</span>
)

const config = {
  before: Before,
  after: After
}

export default config
