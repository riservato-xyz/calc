import "./image.css";

import { Icon } from '@iconify/react';

const CardImage = (props) => {

  const {
    iconID,
    cardTitle,
    cardSubtitle,
    description,
    modifiers
  } = props;

  return (
    <div className={`card-image ${modifiers}`}>
      <aside className={`_featured`}>
        <Icon 
          width="5rem"
          icon={iconID} />
      </aside>

      <main className="_body">
        <header className="_header">
          <h1 className="_title">{cardTitle}</h1>
          {cardSubtitle && <h2 className="_subtitle">{cardSubtitle}</h2>}
        </header>

        <p className="_text">{description}</p>
      </main>
    </div>
  );
};

export default CardImage;
