import "./text.css";

const CardText = (props) => {

  let modifiers = "";

  const {
    featured,
    title,
    description,
    rtl
  } = props;

  modifiers += rtl ? " --rtl" : "";

  return (
    <div className={`card-text ${modifiers}`}>
      <aside className="_featured">
        {featured}
      </aside>

      <main className="_body">
        <header className="_header">
          <h1 className="_title">{title}</h1>
        </header>

        <p className="_text">{description}</p>
      </main>
    </div>
  );
};

export default CardText;
