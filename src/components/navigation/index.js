import "./style.css";

const Navigation = (props) => {

  let prevButton, nextButton, modifiers;

  let { buttons } = props;

  const {
    hasPrev,
    isLast,
    prev,
    jump,
    next,
    disabled
  } = props;

  const nextOrRestart = () => isLast() ? jump(1) : next()

  modifiers += disabled ? ' --disabled' : "";

  buttons = buttons ?? { prev: "voltar", next: "avançar" };

  prevButton = <button className="_button"
    onClick={prev}>{buttons.prev}</button>

  nextButton = <button className={`_button ${modifiers}`}
    disabled={disabled}
    onClick={nextOrRestart}>{buttons.next}</button>

  return (
    <div className="navigation">
      {hasPrev() && prevButton}
      {nextButton}
    </div>
  );
}

export default Navigation
