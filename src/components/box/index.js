import React from "react";

import "./style.css";
import Navigation from '../navigation';
import { HeaderShape } from '../shapes';

const Box = (props) => {

  const {
    children,
    title,
    buttons,
    filled
  } = props;

  return (
    <div className="box">
      <header className="_header">
        <h1 className="_title">{title}</h1>
        <HeaderShape></HeaderShape>
      </header>

      <div className="_body">
        {children}
      </div>

      <Navigation buttons={buttons} {...props} disabled={!filled}></Navigation>
    </div>
  );
};

export default Box;
