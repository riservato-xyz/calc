import React from "react";

import "./styles.css";

import {
  ReactComponent as BirthSVG
} from "../../img/icons/birth.svg";

import {
  ReactComponent as FemaleSVG
} from "../../img/icons/female.svg";

import {
  ReactComponent as MaleSVG
} from "../../img/icons/male.svg";

import {
  ReactComponent as HeightSVG
} from "../../img/icons/height.svg";

import {
  ReactComponent as WeightSVG
} from "../../img/icons/weight.svg";

const Icon = (props) => {

  let {
    className,
    active,
    children
  } = props;

  className += " icon";

  if (active) className += " --active";

  return <i className={className}>{children}</i>
}

const BirthIcon = (props) => <Icon {...props}> <BirthSVG /> </Icon>
const FemaleIcon = (props) => <Icon {...props}> <FemaleSVG /> </Icon>
const MaleIcon = (props) => <Icon {...props}> <MaleSVG /> </Icon>
const WeightIcon = (props) => <Icon {...props}> <WeightSVG /> </Icon>
const HeightIcon = (props) => <Icon {...props}> <HeightSVG /> </Icon>

export {
  BirthIcon,
  FemaleIcon,
  MaleIcon,
  HeightIcon,
  WeightIcon,
};
