import React from "react";

import "./append.css";

const InputAppend = (props) => {
  const {
    handleChange
  } = props;

  const {
    id,
    name,
    value,
    type,
    checked,
    label,
    hint
  } = props;

  return (
    <div className="input --append">

      <label htmlFor={id} className="_label">
        {label}
      </label>

      <span className="_field">
        <input
          id={id}
          type={type}
          name={name}
          value={value}
          onChange={handleChange}
          checked={checked} />
      </span>

      <label htmlFor={id} className="_hint">
        {hint}
      </label>
    </div>
  );
};

export default InputAppend;
