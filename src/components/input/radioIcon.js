import React from "react";

const RadioIcon = (props) => {
  const {
    handleChange
  } = props;

  const {
    id,
    name,
    value,
    children,
    checked
  } = props;

  return (
    <div>
      <input
        id={id}
        className="radio --icon"
        type="radio"
        name={name}
        value={value}
        onChange={handleChange}
        checked={checked} />

      <label htmlFor={id}>
        {children}
      </label>
    </div>
  );
};

export default RadioIcon;
