import React from "react";

const RadioButton = (props) => {
  const {
    handleChange
  } = props;

  const {
    id,
    name,
    value,
    checked,
    btnTitle,
    description
  } = props;

  return (
    <div>
      <input
        id={id}
        className="radio --button"
        type="radio"
        name={name}
        value={value}
        onChange={handleChange}
        checked={checked} />

      <label htmlFor={id}>
        {btnTitle} <br />
        {description}
      </label>
    </div>
  );
};

export default RadioButton;
