import React from "react";

import { CardImage } from '../card';

const RadioCard = (props) => {
  const {
    handleChange,
    className,
    id,
    name,
    value,
    featured,
    cardTitle,
    cardSubtitle,
    cardModifiers,
    description,
    checked,
  } = props;

  return (
    <div className={className}>
      <input
        id={id}
        className="radio --card"
        type="radio"
        name={name}
        value={value}
        onChange={handleChange}
        checked={checked} />

      <label htmlFor={id}>
        <CardImage
          modifiers={cardModifiers}
          featured={featured}
          cardTitle={cardTitle}
          cardSubtitle={cardSubtitle}
          description={description}
          {...props}
        />
      </label>
    </div>
  );
};

export default RadioCard;
