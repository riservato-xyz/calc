import React from "react";

import "./styles.css";

import {
  ReactComponent as HeaderSVG
} from "../../img/shapes/header.svg";

const HeaderShape = () => {

  return(
    <div className="container-shape">
      <HeaderSVG className="triangle"/>
      <div className="rect">aaa</div>
    </div>
  )
}

export {
  HeaderShape,
};
